# Configuration Service
A simple service to get an application config.
## API
Look at api.yml for the OpenAPI specification

## System Context
![System Context](img/SystemContext.png)

## Build
Check the [container registry](https://gitlab.com/spotify-light-itsm/configuration-management/container_registry) for an image. Alternatively you can build the image yourself by running `npm run build`.
## Deployment
The service assumes you have a MongoDB instance running. It must be reachable by the host alias 'db'.

### Setup MongoDB
The following lines setup a MongoDB container for the config service
```
docker network create <NETWORK>
docker run -d --name mongodb --network <NETWORK> --net-alias db mongo
```
The Service accesses the database 'application_configs' and uses a collection with the same name.

The format for a config is:
```
{
    "service": <SERVICE NAME>
    "config": {
        "key": <VALUE>
    }
}
```
### Deploy the Configuration Service
To run the service:
```
docker run -d --name config_service --network <NETWORK> -p <PORT>:8761 registry.gitlab.com/spotify-light-itsm/configuration-management
```

## Working with Spring Config
Spring config should work out of the box when using this service. If not open a [new Issue](https://gitlab.com/spotify-light-itsm/configuration-management/-/issues/new).