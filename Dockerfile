FROM node:12 as build

WORKDIR /usr/src/app

COPY * ./
RUN npm install -D
RUN npm run buildTsc

FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY --from=build /usr/src/app/dist ./
COPY --from=build /usr/src/app/wait-for-it.sh ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

ENV DB_ADDRESS=db
ENV DB_PORT=27017
ENV DB_COLLECTION=application_configs

EXPOSE 8080
RUN chmod +x ./wait-for-it.sh
CMD ./wait-for-it.sh $DB_ADDRESS:$DB_PORT -- node index.js
