import fs from "fs";
import path from "path";
import Database from "../Database";

export default class DirectoryConfigLoader {
    constructor(private configPath: string) {}
    /**
     * Loads the json files in the specified directory
     *
     * @returns Promise
     */
    public async load(): Promise<void> {
        console.log(`Reading directory ${this.configPath}...`);
        fs.readdir(
            path.join(this.configPath),
            (err: any, files: string[]) => {
                console.log(
                    `Found files: ${JSON.stringify(files, null, 2)}`
                );

                const docs = files.map((file: string) => {
                    const configLocation = path.join(
                        this.configPath,
                        file
                    );
                    const content = fs
                        .readFileSync(configLocation)
                        .toString();
                    try {
                        return JSON.parse(content);
                    } catch (e) {
                        console.error(e);
                        return undefined;
                    }
                });
                docs.forEach(async (doc) => {
                    const configExists = await Database.configModel.exists(
                        {
                            service: doc.service,
                        }
                    );
                    if (configExists) {
                        return;
                    }

                    console.log(
                        `Creating default config for service ${doc.service}...`
                    );
                    await Database.configModel.create(doc);
                });
            }
        );
    }
}
