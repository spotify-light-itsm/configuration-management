import Database from "./Database";
import ExpressLoader from "./ExpressLoader";
import dotenv from "dotenv";
dotenv.config();

const database = Database.getInstance();

database.init().then(async () => {
    await database.loadDefaultConfigs();

    const expressLoader = ExpressLoader.getInstance();
    expressLoader.initalize();
    expressLoader.listen();
});
