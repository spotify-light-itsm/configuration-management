import express from "express";
import HomeRouter from "./routers/HomeRouter";
import cors from "cors";
import bodyParser from "body-parser";

export default class ExpressLoader {
    private static instance: ExpressLoader;

    public static getInstance(): ExpressLoader {
        if (!ExpressLoader.instance) {
            ExpressLoader.instance = new ExpressLoader();
        }
        return ExpressLoader.instance;
    }

    private express = express();

    /**
     * Initalizes the express instance
     * @returns {void}
     */
    public initalize(): void {
        this.express.use(cors());
        this.express.use(bodyParser.json());
        this.express.use((req, res, next) => {
            console.log(`${req.method} ${req.path}`);
            next();
        });

        this.express.use(new HomeRouter().expressRouter);
    }
    /**
     * Starts up the express server
     * @returns {void}
     */
    public listen(): void {
        this.express.listen(8080, async () => {
            console.log("Service started on port 8080");
        });
    }
}
