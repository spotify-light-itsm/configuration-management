import Config from "./types/Config";
import { Schema, model, connect, Model } from "mongoose";
import { exit } from "process";
import DirectoryConfigLoader from "./util/DirectoryConfigLoader";

export default class Database {
    private static instance: Database;
    public static configModel: Model<any, any>;
    /**
     * Gets a database instance
     *
     * @returns {Database} Database instance
     */
    public static getInstance(): Database {
        if (!Database.instance) {
            const DB_ADDRESS = process.env.DB_ADDRESS as string;
            const DB_PORT = Number.parseInt(
                process.env.DB_PORT as string
            );
            const DB_COLLECTION = process.env.DB_COLLECTION as string;
            if (!DB_ADDRESS || !DB_PORT || !DB_COLLECTION) {
                console.error("Missing environment variables!");
                exit(1);
            }

            Database.instance = new Database(
                DB_ADDRESS,
                DB_PORT,
                DB_COLLECTION
            );
        }
        return Database.instance;
    }

    /**
     * Creates a database instance with the specifier properties
     *
     * @param  {string} address
     * @param  {number} port
     * @param  {string} collection
     */
    constructor(
        private address: string,
        private port: number,
        private collection: string
    ) {}
    /**
     * Initalizes the database connection
     *
     * @returns Promise
     */
    async init(): Promise<void> {
        const configSchema = new Schema({
            service: {
                type: String,
                required: true,
                index: true,
            },
            config: Object,
        });
        Database.configModel = model(
            "application_configs",
            configSchema
        );
        const CONNECTION_STRING = `mongodb://${this.address}:${this.port}/${this.collection}`;
        console.log(`Trying to connect to ${CONNECTION_STRING}...`);

        try {
            await connect(CONNECTION_STRING, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
        } catch (e) {
            console.error(`Error connection to ${CONNECTION_STRING}`);
            return;
        }
        console.log(`Successfully connected to ${CONNECTION_STRING}`);
    }

    /**
     * Gets a config
     *
     * @param  {string} service
     * @param  {string} profile
     * @returns Promise
     */
    async getConfig(
        service: string,
        profile: string
    ): Promise<Config | null> {
        const configInstance = await Database.configModel.findOne({
            service: service,
        });
        if (!configInstance) {
            return null;
        }

        return {
            name: service,
            profiles: [profile],
            label: null,
            version: null,
            state: null,
            propertySources: [
                {
                    name: profile,
                    source: configInstance.config,
                },
            ],
        };
    }

    /**
     * Loads the configs in the "configs" directory
     *
     * @returns Promise
     */
    public async loadDefaultConfigs(): Promise<void> {
        await new DirectoryConfigLoader("./configs/").load();
    }
}
