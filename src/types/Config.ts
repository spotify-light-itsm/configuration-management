export default interface Config {
    name: string;
    profiles: string[];
    label: null;
    version: null;
    state: null;
    propertySources: [
        {
            name: string;
            source: any;
        }
    ];
}
