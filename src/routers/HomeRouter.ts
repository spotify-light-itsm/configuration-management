import { Router, Request, Response } from "express";
import Database from "../Database";

export default class HomeRouter {
    public expressRouter: Router;

    constructor() {
        this.expressRouter = Router();
        this.initalizeRoutes();
    }
    /**
     * Initalizes the home router.
     * Adds these routes:
     *  - /:serviceName/:type
     *
     * @returns void
     */
    private initalizeRoutes(): void {
        this.expressRouter.get(
            "/:serviceName/:type",
            async (req: Request, res: Response) => {
                const serviceName = req.params.serviceName;
                const profile = req.params.type || "default";

                if (!serviceName) {
                    res.sendStatus(400);
                    return;
                }

                const config = await Database.getInstance().getConfig(
                    serviceName,
                    profile
                );

                if (!config) {
                    res.sendStatus(404);
                    return;
                }

                res.json(config);
            }
        );
    }
}
